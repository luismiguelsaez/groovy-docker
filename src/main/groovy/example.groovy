import de.gesellix.docker.client.DockerClientImpl
import de.gesellix.docker.client.DockerAsyncCallback


def callback = new DockerAsyncCallback() {

    def lines = []

    @Override
    def onEvent(Object line) {
        println(line)
        lines << line
    }

    @Override
    def onFinish() {
    }

}

def dockerClient = new DockerClientImpl("unix:///var/run/docker.sock")

def image = "alpine"
def tag = "3.9"
def cmds = ["apk", "-U", "add", "bash", "curl"]
def config = ["Cmd" : cmds]

def containerStatus = dockerClient.run(image, config, tag)
def containerId = containerStatus.container.content.Id


def response = dockerClient.logs(containerId, [tail: 1], callback)


dockerClient.stop(containerId)
dockerClient.wait(containerId)
dockerClient.rm(containerId)

System.exit(0)