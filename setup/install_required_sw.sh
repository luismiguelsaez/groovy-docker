#!/bin/bash

java -version || echo "Please install Java"
command -v curl || sudo apt-get install curl
SDK_MAN_INIT="$HOME/.sdkman/bin/sdkman-init.sh"

if [[ ! -f "$SDK_MAN_INIT" ]]; then
    echo "Installing SDKMAN"
    curl -s "https://get.sdkman.io" | bash
fi

. $SDK_MAN_INIT
sdk version
sdk install gradle
sdk install groovy

gradle --version
groovy -version
