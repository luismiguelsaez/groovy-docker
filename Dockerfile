FROM gradle:5.3.1-jdk8-alpine AS builder

USER root

RUN mkdir /build
COPY . /build
WORKDIR /build

RUN ./gradlew getDeps build



FROM groovy:jdk8-alpine

ENV CLASSPATH=/app/runtime/*

USER root

RUN mkdir -p /app/src /app/runtime
COPY --from=builder /build/src/ /app/src/
COPY --from=builder /build/runtime/ /app/runtime/
WORKDIR /app

ENTRYPOINT ["groovy","src/main/groovy/example.groovy"]
